<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

// kodingan sheep
$sheep = new Animal("shaun");
$sheep->setlegs(4);
echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->getlegs() . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"

// kodingan frogs
$kodok = new Frog("buduk");
$kodok->setlegs(4);
echo "Name : " . $kodok->name . "<br>"; 
echo "Legs : " . $kodok->getlegs() . "<br>"; 
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
echo "Jump : ".$kodok->jump() . "<br><br>";


// kodigngan ape 
$sungokong = new Ape("kera sakti");
$sungokong->setlegs(2);
echo "Name : " . $sungokong->name . "<br>"; 
echo "Legs : " . $sungokong->getlegs() . "<br>"; 
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; 
echo "Yell : ".$sungokong->yell() . "<br><br>";
?>