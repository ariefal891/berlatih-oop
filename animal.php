<?php

class Animal {
    public $name;
    private $legs;
    public $cold_blooded = "no";

    public function __construct($nama)
    {
        $this->name = $nama;
    }

    public function setlegs($jumlah){
        $this->legs= $jumlah ;
    }

    public function getlegs(){
        return  $this->legs;
    }
}

?>